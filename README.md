# Supply Chain Certificate with Hyperledger Fabric

I file presenti in questo repository permettono la simulazione di una blockchain per la tracciabilità dei prodotti lungo una catena logistica, con lo scopo di certificare ogni fase produttiva e prevenire fenomeni di contraffazione. 

Questa documentazione è stata sviluppata utilizzando Hyperledger Fabric nella versione 1.1, installato su una macchina virtuale Ubuntu 16.04. Se si prevede l’utilizzo di quest’ultima, si consiglia di dedicare almeno 20 GB di archiviazione complessivi per il sistema operativo e l'installazione dello strumento. La rete blockchain di Hyperledger Fabric qui descritta è configurata utilizzando i contenitori docker, con le varie organizzazioni sulla stessa macchina; ovviamente, nel mondo reale, si troveranno in reti o domini IP separati, oppure in ambienti cloud sicuri.

Si parte con il processo di installazione dello strumento, il quale avviene tramite riga di comando (attraverso il terminale Linux, avviabile tramite la combinazione di tasti Ctrl + Alt + T) utilizzando un account con privilegi di amministratore.

Innanzitutto bisogna aggiornare l'indice apt e installare alcuni pacchetti base per poter scaricare i vari strumenti:

sudo apt-get update  
sudo apt-get install apt-transport-https ca-certificates curl software-properties-common

Hyperledger Fabric utilizza per molti dei suoi componenti il linguaggio di programmazione Go, la cui versione da installare dev'essere superiore alla 1.9.x. Scaricare quindi il pacchetto .tar.gz dal sito ufficiale ed estrarre il contenuto nella directory /usr/local/ utilizzando i seguenti comandi:

tar -xvzf [NOME PACCHETTO].tar.gz  
sudo mv go /usr/local/

Fatto ciò, è necessario impostare le variabili di sistema nel file ~/.bashrc (aprendolo in modifica con l'editore standard attraverso il comando "gedit ~/.bashrc") inserendo in fondo le seguenti righe:

export GOPATH=$HOME/go  
export PATH=$PATH:$GOPATH/bin

Come altro prerequisito è necessario aver installato Docker (versione 17.06.2-ce o superiore) e Docker Compose (versione 1.14.0 o superiore). Il primo viene utilizzato per gestire singoli contenitori, mentre il secondo può essere impiegato per la gestione di un'applicazione multi-contenitore. I passaggi da eseguire per la loro installazione sono:

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -  
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb release -cs) stable"  
sudo apt-get update  
sudo apt-get install docker-ce  
sudo curl -L "https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m)" –o /usr/local/bin/docker-compose  
sudo chmod +x /usr/local/bin/docker-compose  
sudo usermod -a -G docker $USER  

Quest'ultimo comando aggiunge l'utente corrente al gruppo docker, ma per rendere effettiva questa modifica è necessario riavviare il sistema prima di proseguire. La verifica della corretta installazione può essere svolta avviando una semplice docker image chiamata hello-world:

docker run hello-world

Per accertarsi che siano state installate le corrette versioni dei vari tool è possibile eseguirli accompagnati dall'opzione --version. Qualora si volessero sviluppare applicazioni per Hyperledger Fabric utilizzando l'SDK per Node.js, sarà necessario disporre della versione 8.9.x di Node.js installata insieme a Python 2.7, ma nello sviluppo di questo progetto non è stato necessario.

Sono invece fondamentali gli Hyperledger Fabric platform-specific binaries, ovvero i file necessari all'esecuzione dello strumento, i quali, dopo essersi posizionati in una cartella a scelta usando il comando \cd", possono essere scaricati e installati nel seguente modo:

curl -sSL https://goo.gl/6wtTN5 | bash -s 1.1.0

Nella directory bin contenuta all'interno della cartella "fabric-samples", si dovrebbero a questo punto trovare i seguenti file: cryptogen, configtxgen, configtxlator, peer, orderer, fabric-ca-client.

Si conclude così l’installazione di Hyperledger Fabric. Scaricare quindi i file presenti in questo repository assicurandosi che la cartella "sc-model" sia posizionata all'interno della directory "fabric-sample", mentre la cartella "supply_chain" dev'essere inserita nella sottodirectory "chaincode".

Le varie istruzioni per l'esecuzione del modello sono state raggruppate in script .sh, suddivisi secondo il tipo di attività che svolgono:
- env.sh: contiene la definizione di tutte le variabili e funzioni comuni a più script.
- start.sh: prepara l'ambiente di esecuzione, eliminando i vecchi contenitori docker per poi passare l'esecuzione a makeDocker.sh.
- makeDocker.sh: crea i nuovi container, chiamando in sequenza gli script start-root-ca.sh, start-intermediate-ca.sh, setup-fabric.sh, startorderer.sh, start-peer.sh e run-fabric.sh, per poi comporre il file dockercompose.yml.
- start-root-ca.sh: crea i file di sistema e avvia i servizi utilizzati dalle root Certificate Authorities.
- start-intermediate-ca.sh: crea i file di sistema e avvia i servizi utilizzati dalle intermediate Certificate Authorities.
- setup-fabric.sh: genera i certificati delle varie organizzazioni e dei relativi utenti, oltre a creare i blocchi iniziali.
- start-orderer.sh: crea i file di sistema e avvia i servizi utilizzati dall'Ordering Service.
- start-peer.sh: crea i file di sistema e avvia i servizi utilizzati dai peer.
- run-fabric.sh: crea i canali, installa i chaincode, esegue invoke e query sui vari peer.
- stop.sh: termina l'esecuzione dei contenitori docker.

Per eseguire l'esempio sviluppato in questo progetto basterà quindi avviare il file start.sh a cui sono collegati tutti gli altri script. Lo scenario implementato è composto da 5 partecipanti che operano su un unico canale chiamato "supplychain": due produttori (prod1 e prod2), un assemblatore (integr), un venditore (vendit) e due rivenditori (riven1 e riven2). Ognuno di essi costituisce un'organizzazione con associato un peer, il quale ha la propria Certificate Authority (CA). Qualora si volessero modificare i nomi o il numero delle organizzazioni, piuttosto che la quantità di peer associati a ciascuna di esse, basterà cambiare i valori delle relative variabili presenti nel file env.sh. Si precisa però che l'aumento del numero di peer comporta un maggior tempo di esecuzione, che varia comunque a seconda dell'hardware della macchina su cui gira lo strumento: si è testato che il funzionamento con due peer per ciascun membro ha raddoppiato i tempi di esecuzione che si hanno con peer singoli.

Nel file run-fabric.sh è presente una simulazione della supply chain. I due produttori creano due componenti (product1 e product2) e li trasferiscono all'assemblatore, il quale crea un prodotto (product3) a cui assegnare le materie prime ricevute. Inoltre, il venditore che è interessato a product3, decide di verificare la sua storia prima di acquistarlo presso il fornitore. Si simula inoltre che, per una certa ragione, viene revocato il certificato ad un rivenditore. Durante lo svolgimento vengono visualizzati a video i passaggi che vengono eseguiti e viene creato un file di log contenente tutti i dettagli del processo. Qualora si verifichi un errore l'esecuzione si arresta ed è possibile ricercare il problema che lo ha causato all'interno del file di log.
