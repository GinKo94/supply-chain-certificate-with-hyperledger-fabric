package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/hyperledger/fabric/core/chaincode/lib/cid"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

type SimpleChaincode struct {
}

type product struct {
	UID	string `json:"id"`
	Descr	string `json:"descr"`
	Weight	int    `json:"weight"`
	Owner	string `json:"owner"`
	Items	[]string `json:"items"`
}

// Main
func main() {
	err := shim.Start(new(SimpleChaincode))
	if err != nil {
		fmt.Printf("Error starting Simple chaincode: %s", err)
	}
}

// Init initializes chaincode
func (t *SimpleChaincode) Init(stub shim.ChaincodeStubInterface) pb.Response {
	return shim.Success(nil)
}

// Invoke
func (t *SimpleChaincode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	function, args := stub.GetFunctionAndParameters()
	fmt.Println("invoke is running " + function)

	if function == "create" {
		return t.initProduct(stub, args)
	} else if function == "transfer" {
		return t.transferProduct(stub, args)
	} else if function == "merge" {
		return t.addComponent(stub, args)
	} else if function == "read" {
		return t.readProduct(stub, args)
	} else if function == "history" {
		return t.getHistoryForProduct(stub, args)
	}

	fmt.Println("invoke did not find func: " + function)
	return shim.Error("Received unknown function invocation")
}

// initProduct - create a new product, store into chaincode state
func (t *SimpleChaincode) initProduct(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	var err error

	if len(args) != 3 {
		return shim.Error("Incorrect number of arguments. Expecting 3")
	}

	fmt.Println("- start init product")
	if len(args[0]) <= 0 {
		return shim.Error("1st argument must be a non-empty value")
	}
	if len(args[1]) <= 0 {
		return shim.Error("2nd argument must be a non-empty value")
	}
	if len(args[2]) <= 0 {
		return shim.Error("3rd argument must be a non-empty value")
	}

	mspid, err := cid.GetMSPID(stub)
	if err != nil {
		return shim.Error(err.Error())
	}

	productName := args[0]
	items := []string{}
	descr := strings.ToLower(args[1])
	owner := mspid[:len(mspid)-3]
	weight, err := strconv.Atoi(args[2])
	if err != nil {
		return shim.Error("3rd argument must be an integer")
	}

	productAsBytes, err := stub.GetState(productName)
	if err != nil {
		return shim.Error("Failed to get product: " + err.Error())
	} else if productAsBytes != nil {
		fmt.Println("This product already exists: " + productName)
		return shim.Error("This product already exists: " + productName)
	}

	product := &product{productName, descr, weight, owner, items}
	productJSONasBytes, err := json.Marshal(product)
	if err != nil {
		return shim.Error(err.Error())
	}
	err = stub.PutState(productName, productJSONasBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	// Return success ====
	fmt.Println("- end init product")
	return shim.Success(nil)
}

// readProduct - read a product from chaincode state
func (t *SimpleChaincode) readProduct(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	var name, jsonResp string
	var err error

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting name of the product to query")
	}

	name = args[0]
	valAsbytes, err := stub.GetState(name)
	if err != nil {
		jsonResp = "{\"Error\":\"Failed to get state for " + name + "\"}"
		return shim.Error(jsonResp)
	} else if valAsbytes == nil {
		jsonResp = "{\"Error\":\"Product does not exist: " + name + "\"}"
		return shim.Error(jsonResp)
	}

	return shim.Success(valAsbytes)
}

// transfer a product by setting a new owner name on the product
func (t *SimpleChaincode) transferProduct(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	if len(args) < 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2")
	}

	mspid, err := cid.GetMSPID(stub)
	if err != nil {
		return shim.Error(err.Error())
	}

	productName := args[0]
	newOwner := strings.ToLower(args[1])
	fmt.Println("- start transferProduct ", productName, newOwner)

	productAsBytes, err := stub.GetState(productName)
	if err != nil {
		return shim.Error("Failed to get product: " + err.Error())
	} else if productAsBytes == nil {
		return shim.Error("Product does not exist: " + productName)
	}

	productToTransfer := product{}
	err = json.Unmarshal(productAsBytes, &productToTransfer)
	if err != nil {
		return shim.Error(err.Error())
	}

	if productToTransfer.Owner != mspid[:len(mspid)-3] {
		return shim.Error("Only the current owner can transfer the product")
	}

	productToTransfer.Owner = newOwner

	productJSONasBytes, _ := json.Marshal(productToTransfer)
	err = stub.PutState(productName, productJSONasBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	fmt.Println("- end transferProduct (success)")
	return shim.Success(nil)
}

// add component (item) to a product
func (t *SimpleChaincode) addComponent(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	if len(args) < 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2")
	}

	mspid, err := cid.GetMSPID(stub)
	if err != nil {
		return shim.Error(err.Error())
	}

	productName := args[0]
	component := strings.ToLower(args[1])
	fmt.Println("- start addComponent ", productName, component)

	productAsBytes, err := stub.GetState(productName)
	if err != nil {
		return shim.Error("Failed to get product:" + err.Error())
	} else if productAsBytes == nil {
		return shim.Error("Product does not exist")
	}

	componentAsBytes, err := stub.GetState(component)
	if err != nil {
		return shim.Error("Failed to get component:" + err.Error())
	} else if componentAsBytes == nil {
		return shim.Error("Component does not exist")
	}

	finalProduct := product{}
	err = json.Unmarshal(productAsBytes, &finalProduct)
	if err != nil {
		return shim.Error(err.Error())
	}

	componentResultsIterator, err := stub.GetStateByPartialCompositeKey("items~name", []string{component})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer componentResultsIterator.Close()

	if componentResultsIterator.HasNext(){
		return shim.Error("Component not available (used in another product)")
	}

	if mspid[:len(mspid)-3] != "integr" {
		return shim.Error("Only the integrator can add a product")
	}
	
	finalProduct.Items = append(finalProduct.Items, component)

	productJSONasBytes, _ := json.Marshal(finalProduct)
	err = stub.PutState(productName, productJSONasBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	indexName := "items~name"
	indexKey, err := stub.CreateCompositeKey(indexName, []string{component, productName})
	if err != nil {
		return shim.Error(err.Error())
	}
	value := []byte{0x00}
	stub.PutState(indexKey, value)

	fmt.Println("- end addComponent (success)")
	return shim.Success(nil)
}

// Query the ledger for a product's history
func (t *SimpleChaincode) getHistoryForProduct(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	if len(args) < 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	productName := args[0]

	fmt.Printf("- start getHistoryForProduct: %s\n", productName)

	resultsIterator, err := stub.GetHistoryForKey(productName)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		response, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		
		buffer.WriteString("{\"TxId\":")
		buffer.WriteString("\"")
		buffer.WriteString(response.TxId)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Value\":")
		if response.IsDelete {
			buffer.WriteString("null")
		} else {
			buffer.WriteString(string(response.Value))
		}

		buffer.WriteString(", \"Timestamp\":")
		buffer.WriteString("\"")
		buffer.WriteString(time.Unix(response.Timestamp.Seconds, int64(response.Timestamp.Nanos)).String())
		buffer.WriteString("\"")

		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	fmt.Printf("- getHistoryForProduct returning:\n%s\n", buffer.String())

	return shim.Success(buffer.Bytes())
}
